# from __future__ import print_function
#import scipy
#from scipy.sparse import issparse
import keras
from keras.models import Sequential
#from keras.layers import Dense, Dropout, Flatten, Reshape, Activation
from keras.layers import Dense
from keras import backend as K
#import enigma_data
import pickle
from numpy import *
# from ann_visualizer.visualize import ann_viz
from keras.wrappers.scikit_learn import KerasClassifier
# from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
# from sklearn.preprocessing import LabelEncoder
# from sklearn.pipeline import Pipeline


with open('/global/cfs/projectdirs/lz/users/jyjohnson/s2_pulses_5k_phd_input_nn_v2.pickle', 'rb') as handle:
    s2_pulses_input_matrix = pickle.load(handle)

batch_size = 128
epochs = 10

n_feature = shape(s2_pulses_input_matrix[:,0:-1])[1] # length of pulse features shape


x_data = s2_pulses_input_matrix[:,0:-1] #top 20 PMTs radius and timing data
y_data = s2_pulses_input_matrix[:,-1]/1000. #drift time in us

'''fiducialize drift time; in= 200-600us'''

# bin_y_data=[]
# # class_bins=arange(0,900,200)
# class_bins=array([0,200,600,800])
# num_classes=len(class_bins)-1
# det_bin=digitize(y_data,class_bins)
# dt_cut=(det_bin<max(det_bin)) #to remove any events that have drift times >850us (unrealistic)

# for ii in range(len(y_data[dt_cut])):
#     class_id_array=zeros(num_classes)
#     class_id_array[det_bin[dt_cut][ii]-1] = 1
#     bin_y_data.append(class_id_array)
# y_data=array(bin_y_data)
# x_data=x_data[dt_cut]

dt_cut= (y_data<820)
bin_y_data=[]
for ii in range(len(y_data[dt_cut])):
    if y_data[dt_cut][ii]>=200 and y_data[dt_cut][ii]<=600 :
        bin_y_data.append(array([1,0]))
    else:
        bin_y_data.append(array([0,1]))
y_data=array(bin_y_data)
x_data=x_data[dt_cut]


'''Building neural network'''

def baseline_model():
	# create model
    model = Sequential()
    model.add(Dense(n_feature+1,init='normal', activation='relu', input_dim=n_feature))
    model.add(Dense(n_feature+1,init='normal', activation='relu'))
    model.add(Dense(int(n_feature/2), init='normal', activation='relu'))
    model.add(Dense(int(n_feature/6), init='normal', activation='relu'))
    model.add(Dense(2, init='normal', activation='softmax'))
	# Compile model
    model.compile(loss=keras.losses.binary_crossentropy,optimizer=keras.optimizers.Adam(learning_rate=0.001,beta_1=0.9, beta_2=0.999,amsgrad=False),metrics=['accuracy'])
    return model

'''Testing neural network'''

estimator = KerasClassifier(build_fn=baseline_model, epochs=epochs, batch_size=batch_size, verbose=1)
kfold = KFold(n_splits=10, shuffle=True)

'''Fit neural network on data'''

results = cross_val_score(estimator, x_data, y_data, cv=kfold)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))
# print(results)