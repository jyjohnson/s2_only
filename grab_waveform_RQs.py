# from matplotlib import *
# import matplotlib.pyplot as plt
import os
import uproot
import logging
from scipy.integrate import cumtrapz
from numpy import *
from time import process_time as timer
import pickle

def setup_logger(name, log_file, level=logging.DEBUG):
    handler = logging.FileHandler(log_file, mode='a')        
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

def load_data(data_file, pulses_RQ_list, ss_RQ_list, event_RQ_list, calibratedPods_RQ_list, waveforms_RQ_list):
#     files = []
#     # r=root, d=directories, f = files
#     for r, d, f in os.walk(data_path):
#         for file in f:
#             if ('.root' in file):
#                 files.append(os.path.join(r, file))             
    d={}
    e={}
    g={}
    m={}
#     h={}
    k={}
   
    for key in ss_RQ_list:
        d[key]=array([])
    for key in pulses_RQ_list:
        e[key]=array([])
    for key in event_RQ_list:
        g[key]=array([])
    for key in waveforms_RQ_list:
#         h[key]=array([])
        k[key]=array([])
    for key in calibratedPods_RQ_list:
        m[key]=array([])
#     for ii,f in enumerate(files):
#         print('{:.3g}'.format(floor(1000*ii/len(files))/10), end =" ")
    tmp=uproot.open(data_file)
    S=tmp['Scatters']
    P=tmp['Events']
    M=tmp['PodWaveforms']
    for key in ss_RQ_list:
        d[key]=append(d[key],S.arrays(['ss.'+key])[bytes('ss.'+key, encoding='utf-8')])
    for key in pulsesTPC_RQ_list:
        e[key]=append(e[key],P.arrays(['pulsesTPC.'+key])[bytes('pulsesTPC.'+key, encoding='utf-8')])
    for key in event_RQ_list:
        g[key]=append(g[key],P.arrays(['eventHeader.'+key])[bytes('eventHeader.'+key, encoding='utf-8')])
    for key in waveforms_RQ_list:
#         h[key]=append(h[key],M.arrays(['summedPodsTPCLG.'+key])[bytes('summedPodsTPCLG.'+key, encoding='utf-8')])
        k[key]=append(k[key],M.arrays(['summedPodsTPCHG.'+key])[bytes('summedPodsTPCHG.'+key, encoding='utf-8')])
    for key in calibratedPods_RQ_list:
        m[key]=append(m[key],M.arrays(['calibratedPods.'+key])[bytes('calibratedPods.'+key, encoding='utf-8')])
    return(e,d,g,m,k)

if __name__ == '__main__':
    
    formatter=logging.Formatter('%(levelname)s %(asctime)s %(name)s: %(message)s','%m/%d/%Y %I:%M:%S %p')
    exception_traceback_log=setup_logger('Exception Traceback','exception_handling_monitor.log')
    
    initial=timer()
    ss_RQ_list=['s2Area_phd','driftTime_ns','nSingleScatters','s2PulseID']
    event_RQ_list=['runID','eventID', 'rawFileName']
    pulsesTPC_RQ_list=['pulseID','pulseStartTime_ns','pulseEndTime_ns','pulseArea_phd','rmsWidth_ns','fwhm_ns',
                       's2Probability', 'singleElectronProbability','s2Xposition_cm','s2Yposition_cm']
    waveforms_RQ_list=['podStartTime_ns', 'podSamples']
    calibratedPods_RQ_list=['podStartTime_ns', 'nPods', 'podSamples', 'channelID']

    reprocessed_files_dir="/global/cfs/projectdirs/lz/users/jyjohnson/s2_pulse_lib_files_5k_phd/" 
    files = []
    # r=root, x=directories, z = files
    for r, x, z in os.walk(reprocessed_files_dir):
        for file in z:
            if ('.root' in file) and ('mctruth' not in file):
                files.append(os.path.join(r, file))  

    TOP_channel_count=252
    LG=False
    max_num_entries=20
    s2_pulses_lib={}
    s2_keys_list=[pulsesTPC_RQ_list,ss_RQ_list,event_RQ_list]
    for _list in s2_keys_list:
        for key in _list:
            s2_pulses_lib[key]=[]
    s2_pulses_lib['channelWaveformArea_phd']=[]
    s2_pulses_lib['channelWaveform']=[]
    s2_pulses_lib['channelIDNum']=[]
    s2_pulses_lib['summedWaveform']=[]

    print('Starting to grab waveforms from files in: %s'%(reprocessed_files_dir))

    evt_count=0
    for ii,file in enumerate(files):
        start=timer()
        e,d,g,m,k=load_data(file, pulsesTPC_RQ_list, ss_RQ_list, event_RQ_list, calibratedPods_RQ_list, waveforms_RQ_list)
        
        if len(e['pulseID'])<95:
            end=timer()
            print('Root File %s/%s has too few entries; skipping file.'%(ii+1,len(files)),end='\n')
            continue
        
        end=timer()
        print('Loaded File %s/%s: %s'%(ii+1,len(files),file),end=' ')
        print('Time Elapsed to load file: %s seconds'%(end-start))

        t0=timer()
        for event in range(len(m['podSamples'])):
            if d['nSingleScatters'][event]==1:
                evt_count+=1
#                 if evt_count%100==0:
#                     print('Processed %s events'%(evt_count))

                pulse=int(d['s2PulseID'][event])
                
                '''The below is for grabbing the summed waveform for a given low E S2 pulse'''
                pod_index=0
                while pod_index<len(k['podStartTime_ns'][event]):
                    pod_end=k['podStartTime_ns'][event][pod_index]+len(k['podSamples'][event][pod_index])*10
                    if (k['podStartTime_ns'][event][pod_index]<=e['pulseEndTime_ns'][event][pulse]<=pod_end) & (k['podStartTime_ns'][event][pod_index]<=e['pulseStartTime_ns'][event][pulse]<=pod_end):
                        break
                    else:
                        pod_index+=1
                if pod_index<len(k['podStartTime_ns'][event]):
                    start=int((e['pulseStartTime_ns'][event][pulse]-k['podStartTime_ns'][event][pod_index])/10)
                    end=start+int((e['pulseEndTime_ns'][event][pulse]-e['pulseStartTime_ns'][event][pulse])/10)
                    summed_waveform=k['podSamples'][event][pod_index][start:end]
                else:
                    summed_waveform=[-999]
                
                '''The below is for grabbing channel waveforms for top 20 PMTs'''
                tmp_dict={}
                tmp_dict['channelWaveformArea_phd']=[]
                tmp_dict['channelWaveform']=[]
                tmp_dict['channelIDNum']=[]

                
                ps_array=array(m['podSamples'][event])
                if LG==True:
                    tmp_ch_ids=unique(m['channelID'][event])[(unique(m['channelID'][event])>=1000)&(unique(m['channelID'][event])<=TOP_channel_count+1000)]
                elif LG==False:
                    tmp_ch_ids=unique(m['channelID'][event])[(unique(m['channelID'][event])<=TOP_channel_count)]
                for ch in tmp_ch_ids:
                    channel_cut=(m['channelID'][event]==ch)
                    tmp_st=m['podStartTime_ns'][event][channel_cut]
                    tmp_ps=ps_array[channel_cut]
                    init_cut=(tmp_st<=e['pulseStartTime_ns'][event][pulse])
                    c_tmp_st=tmp_st[init_cut]
                    c_tmp_ps=tmp_ps[init_cut]
                    if len(c_tmp_st)==0:
                        init_index=0
#                         tmp_st=tmp_st[init_index]
                        tmp_ps[init_index]=array(tmp_ps[init_index])
                        init_pod_end=tmp_st[init_index]+len(tmp_ps[init_index])*10
                        if int(init_pod_end) <= int(e['pulseEndTime_ns'][event][pulse]):
                            len_pad_zeros_beg=int(int(tmp_st[init_index]-e['pulseStartTime_ns'][event][pulse])/10)
                            len_pad_zeros_end=int(int(e['pulseEndTime_ns'][event][pulse]-init_pod_end)/10)
                            waveform=concatenate((zeros(len_pad_zeros_beg),tmp_ps[init_index],zeros(len_pad_zeros_end)))
#                             waveform_area=cumtrapz(waveform,dx=10)[-1]
                        elif int(init_pod_end) > int(e['pulseEndTime_ns'][event][pulse]):
                            end=int(int(init_pod_end-e['pulseEndTime_ns'][event][pulse])/10)
                            len_pad_zeros_beg=int(int(tmp_st[init_index]-e['pulseStartTime_ns'][event][pulse])/10)
                            waveform=concatenate((zeros(len_pad_zeros_beg),tmp_ps[init_index][:-end]))
#                             waveform_area=cumtrapz(waveform,dx=10)[-1]
                    else:
                        init_index=argwhere(tmp_st==c_tmp_st[-1])[0][0]
                        tmp_ps[init_index]=array(tmp_ps[init_index])
                        init_pod_end=tmp_st[init_index]+len(tmp_ps[init_index])*10
                        if init_index==len(tmp_st)-1:
                            if init_pod_end > e['pulseEndTime_ns'][event][pulse]:
                                start=int(int(e['pulseStartTime_ns'][event][pulse]-tmp_st[init_index])/10)
                                end=start+int(int(e['pulseEndTime_ns'][event][pulse]-e['pulseStartTime_ns'][event][pulse])/10)
                                waveform=tmp_ps[init_index][start:end]
#                                 waveform_area=cumtrapz(waveform,dx=10)[-1]
                            elif init_pod_end <= e['pulseEndTime_ns'][event][pulse]:
                                start=int(int(e['pulseStartTime_ns'][event][pulse]-tmp_st[init_index])/10)
                                len_pad_zeros_end=int(int(e['pulseEndTime_ns'][event][pulse]-init_pod_end)/10)
                                waveform=concatenate((tmp_ps[init_index][start:],zeros(len_pad_zeros_end)))
#                                 waveform_area=cumtrapz(waveform,dx=10)[-1]
                        else:
                            if init_pod_end <= e['pulseStartTime_ns'][event][pulse]:
                                sec_index=init_index+1
                                tmp_ps[sec_index]=array(tmp_ps[sec_index])
                                sec_pod_end=tmp_st[sec_index]+len(tmp_ps[sec_index])*10
                                if tmp_st[sec_index] > e['pulseStartTime_ns'][event][pulse]:
                                    if sec_pod_end > e['pulseEndTime_ns'][event][pulse]:
                                        end=int(int(init_pod_end-e['pulseEndTime_ns'][event][pulse])/10)
                                        len_pad_zeros_beg=int(int(tmp_st[sec_index]-e['pulseStartTime_ns'][event][pulse])/10)
                                        waveform=concatenate((zeros(len_pad_zeros_beg),tmp_ps[sec_index][:-end]))
#                                         waveform_area=cumtrapz(waveform,dx=10)[-1]
                                    elif sec_pod_end <= e['pulseEndTime_ns'][event][pulse]:
                                        len_pad_zeros_beg=int(int(tmp_st[sec_index]-e['pulseStartTime_ns'][event][pulse])/10)
                                        len_pad_zeros_end=int(int(e['pulseEndTime_ns'][event][pulse]-sec_pod_end)/10)
                                        waveform=concatenate((zeros(len_pad_zeros_beg),tmp_ps[sec_index],zeros(len_pad_zeros_end)))
#                                         waveform_area=cumtrapz(waveform,dx=10)[-1]
                            else:
                                if init_pod_end < e['pulseEndTime_ns'][event][pulse]:
                                    start=int(int(e['pulseStartTime_ns'][event][pulse]-tmp_st[init_index])/10)
                                    len_pad_zeros_end=int(int(e['pulseEndTime_ns'][event][pulse]-init_pod_end)/10)
                                    waveform=concatenate((tmp_ps[init_index][start:],zeros(len_pad_zeros_end)))
#                                     waveform_area=cumtrapz(waveform,dx=10)[-1]
                                elif init_pod_end >= e['pulseEndTime_ns'][event][pulse]:
                                    start=int(int(e['pulseStartTime_ns'][event][pulse]-tmp_st[init_index])/10)
                                    end=start+int(int(e['pulseEndTime_ns'][event][pulse]-e['pulseStartTime_ns'][event][pulse])/10)
                                    waveform=tmp_ps[init_index][start:end]
#                                     waveform_area=cumtrapz(waveform,dx=10)[-1]
                    
                    try:
                        waveform_area=cumtrapz(waveform,dx=10)[-1]
                    except Exception:
                        waveform_area=0
                        exception_traceback_log.exception('The following error occurred for %s in %s in %s:'%(ch,event,file))
                        pass
                        
                    if len(tmp_dict['channelWaveformArea_phd'])<max_num_entries:
                            tmp_dict['channelWaveformArea_phd'].append(waveform_area)
                            tmp_dict['channelWaveform'].append(waveform)
                            tmp_dict['channelIDNum'].append(ch)
                    elif len(tmp_dict['channelWaveformArea_phd'])==max_num_entries:
                        if min(tmp_dict['channelWaveformArea_phd'])<waveform_area:
                            min_index=argmin(tmp_dict['channelWaveformArea_phd'])
                            tmp_dict['channelWaveformArea_phd'][min_index]=waveform_area
                            tmp_dict['channelWaveform'][min_index]=waveform
                            tmp_dict['channelIDNum'][min_index]=ch

                s2_pulses_lib['channelWaveformArea_phd'].append(tmp_dict['channelWaveformArea_phd'])
                s2_pulses_lib['channelWaveform'].append(tmp_dict['channelWaveform'])
                s2_pulses_lib['channelIDNum'].append(tmp_dict['channelIDNum'])
                s2_pulses_lib['summedWaveform'].append(summed_waveform)

                for key in pulsesTPC_RQ_list:
                    s2_pulses_lib[key].append(e[key][event][pulse])
                for key in ss_RQ_list:
                    s2_pulses_lib[key].append(d[key][event])
                for key in event_RQ_list:
                    s2_pulses_lib[key].append(g[key][event])
        t1=timer()
        print('Finished processing file %s/%s: %s'%(ii+1,len(files),file),end=' ')
        print('Time Elapsed to process file: %s seconds'%(t1-t0))

        with open('/global/cfs/projectdirs/lz/users/jyjohnson/s2_pulses_5k_phd_v1.pickle', 'wb') as handle:
            pickle.dump(s2_pulses_lib, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print('Updated .pickle file. Total entries: %s'%(evt_count))
    final=timer()
    print("Total time taken for program to run: %s seconds"%(final-initial))