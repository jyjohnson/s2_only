import uproot
import os
import logging
from time import process_time as timer
from numpy import *

def setup_logger(name, log_file, level=logging.DEBUG):
    handler = logging.FileHandler(log_file, mode='a')        
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

def load_data(data_file, ss_RQ_list, event_RQ_list, pulses_TPC_RQ_list):
    
    d={}
    e={}
    g={}
    for key in ss_RQ_list:
        d[key]=array([])
    for key in event_RQ_list:
        g[key]=array([])
    for key in pulses_TPC_RQ_list:
        e[key]=array([])
        
    tmp=uproot.open(data_file)
    S=tmp['Scatters']
    P=tmp['Events']
    for key in ss_RQ_list:
        d[key]=append(d[key],S.arrays(['ss.'+key])[bytes('ss.'+key, encoding='utf-8')])
    for key in event_RQ_list:
        g[key]=append(g[key],P.arrays(['eventHeader.'+key])[bytes('eventHeader.'+key, encoding='utf-8')])
    for key in pulses_TPC_RQ_list:
        e[key]=append(e[key],P.arrays(['pulsesTPC.'+key])[bytes('pulsesTPC.'+key, encoding='utf-8')])
    return(d,e,g)

if __name__ == '__main__':
    
    formatter=logging.Formatter('%(levelname)s %(asctime)s %(name)s: %(message)s','%m/%d/%Y %I:%M:%S %p')
    exception_traceback_log=setup_logger('Exception Traceback','exception_handling_monitor.log')
    
    initial=timer()
    ss_RQ_list=['s2Area_phd','driftTime_ns','nSingleScatters','s2PulseID']
    event_RQ_list=['runID','eventID', 'rawFileName']
    pulses_TPC_RQ_list=['pulseID','s2Probability','singleElectronProbability', 'pulseArea_phd']
    data_path='/global/cfs/cdirs/lz/data/MDC3/background/LZAP-4.6.3/'
    
    max_S2_pulse_area=5000

    files = []
    # r=root, d=directories, f = files
    for r,d,f in os.walk(data_path):
        if 'MCTRUTH' not in r and '2018' in r:
            for file in f:
                if ('.root' in file):
                    files.append(os.path.join(r, file))

    s2_pulses_lib={}
    s2_keys_list=[ss_RQ_list,event_RQ_list,pulses_TPC_RQ_list]
    for _list in s2_keys_list:
        for key in _list:
            s2_pulses_lib[key]=[]
    s2_pulses_lib['date']=[]
    
    evt_count=0
    lines_s2=[]
    for ii,file in enumerate(files):
        start=timer()
        date=file[53:62]
        
        print('Starting to grab areas from file[%s/%s]: %s'%(ii,len(files),file))
        try:
            d,e,g=load_data(file, ss_RQ_list, event_RQ_list, pulses_TPC_RQ_list)
        except Exception:
            exception_traceback_log.exception('The following error occurred for %s:'%(file))
            print('Skipping file due to error with loading.')
            continue
        
        for i in range(len(d['nSingleScatters'])):
            if d['nSingleScatters'][i]==1:
                for j in range(len(e['pulseID'][i])):
                    if e['pulseID'][i][j]==d['s2PulseID'][i]:
                        if e['pulseArea_phd'][i][j]<max_S2_pulse_area:
                            for key in pulses_TPC_RQ_list:
                                s2_pulses_lib[key].append(e[key][i][j])
                            for key in ss_RQ_list:
                                s2_pulses_lib[key].append(d[key][i])
                            for key in event_RQ_list:
                                s2_pulses_lib[key].append(g[key][i])
                            s2_pulses_lib['date'].append(date)
                            
                            evt_count+=1
                            if evt_count%100==0:
                                print("The current event count is: %s" %(evt_count))
        end=timer()
        print('Time Elapsed to load file: %s seconds'%(end-start))
        
        
    raw_data_path='/global/projecta/projectdirs/lz/data/warehouse/MDC3/background/BACCARAT-4.11.0_DER-8.5.13/'
    for i in range(len(s2_pulses_lib['rawFileName'])):
        entry=' '.join((raw_data_path+str(s2_pulses_lib['date'][i])+str(s2_pulses_lib['rawFileName'][i].decode()),str(int(s2_pulses_lib['runID'][i])),str(int(s2_pulses_lib['eventID'][i]))))
        lines_s2.append(entry)
    
    unique_lines_s2=unique(lines_s2)  
    count=0
    output_dir='/global/cfs/projectdirs/lz/users/jyjohnson/parsed_txt_files_s2_5k_phd/'
    for num, entry in enumerate(unique_lines_s2,1):
        if num%100==0:
            count+=1
        filename=output_dir+'parsed_list_files_s2_5k_phd_'+str(count)+'.txt'
        with open(filename,'a') as f:
            f.write(entry+'\n')
        
        
    final=timer()
    print("Total time taken for program to run: %s seconds"%(final-initial))
    print("Total events obtained: %s"%(evt_count))
        