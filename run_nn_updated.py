# from __future__ import print_function
#import scipy
#from scipy.sparse import issparse
import keras
from keras.models import Sequential
#from keras.layers import Dense, Dropout, Flatten, Reshape, Activation
from keras.layers import Dense
from keras import backend as K
#import numpy as np
#import enigma_data
import pickle
from numpy import *
from ann_visualizer.visualize import ann_viz
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score, auc, accuracy_score
from sklearn.preprocessing import StandardScaler
import bisect

with open('/global/cfs/projectdirs/lz/users/jyjohnson/s2_pulses_5k_phd_input_nn_v3.pickle', 'rb') as handle:
    s2_pulses_input_matrix = pickle.load(handle)

batch_size = 128
epochs = 25

n_feature = shape(s2_pulses_input_matrix[:,0:-1])[1] # length of pulse features shape


x_data = s2_pulses_input_matrix[:,0:-1] #top 20 PMTs radius and timing data
y_data = s2_pulses_input_matrix[:,-1]/1000. #drift time in us

'''Bin y data into drift time bins of 50us'''

# bin_y_data=[]
# class_bins=arange(0,900,200)
# num_classes=len(class_bins)-1
# det_bin=digitize(y_data,class_bins)
# dt_cut=(det_bin<max(det_bin)) #to remove any events that have drift times >850us (unrealistic)

# for ii in range(len(y_data[dt_cut])):
#     class_id_array=zeros(num_classes)
#     class_id_array[det_bin[dt_cut][ii]-1] = 1
#     bin_y_data.append(class_id_array)
# y_data=array(bin_y_data)
# x_data=x_data[dt_cut]

'''Fiducialize drift time; inside= 200-600us'''

dt_cut= (y_data<820)&(y_data>0)
bin_y_data=[]
for ii in range(len(y_data[dt_cut])):
    if y_data[dt_cut][ii]>=200 and y_data[dt_cut][ii]<=600:
        bin_y_data.append(array([1,0]))
    else:
        bin_y_data.append(array([0,1]))
y_data=array(bin_y_data)
x_data=x_data[dt_cut]
num_classes=2

'''Divide data into training and test'''

N = len(x_data) #number of generated pulses after cuts (for training and test)
training_frac = 0.85

x_train = x_data[0:int(N*training_frac),:]
x_test = x_data[int(N*training_frac):,:]
y_train = y_data[0:int(N*training_frac)]
y_test = y_data[int(N*training_frac):]
print('X train shape',x_train.shape, 'y train shape',y_train.shape)
x_train_unscaled = x_train.astype('float32')
x_test_unscaled = x_test.astype('float32')

'''Perform initial transformations - StandardScaler sets mean of each feature to 0, std to 1'''

scaler = StandardScaler()
x_train = scaler.fit_transform(x_train_unscaled)
x_test = scaler.transform(x_test_unscaled) # Use same scaling transformation on test and train data (do not pick a new mean, std)



'''Building neural network'''

model = Sequential()
model.add(Dense(n_feature+1,init='random_normal', activation='tanh', input_dim=n_feature))
# model.add(Dense(int(2*n_feature/3),init='random_normal', activation='tanh'))
model.add(Dense(n_feature+1,init='random_normal', activation='tanh'))
model.add(Dense(int(n_feature/2), init='random_normal', activation='tanh'))
model.add(Dense(int(n_feature/6), init='random_normal', activation='tanh'))
model.add(Dense(num_classes, init='random_normal', activation='softmax'))
#model.add(Reshape((n1, n2)))
#model.add(Dense(N_sample, activation='softmax'))

'''Testing neural network'''

# model.compile(loss=keras.losses.binary_crossentropy,
#               optimizer=keras.optimizers.Adadelta(),
#               metrics=['accuracy'])

model.compile(loss=keras.losses.binary_crossentropy,
              optimizer=keras.optimizers.Nadam(learning_rate=0.001, beta_1=0.9, beta_2=0.999),
              metrics=['accuracy'])

'''Fit neural network on data'''

# model.summary()

# model.fit(x_train, y_train,
#           batch_size=batch_size,
#           epochs=epochs,
#           verbose=1,
#           validation_data=(x_test, y_test))

history=model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
         validation_split=0.12)

score = model.evaluate(x_test, y_test, verbose=0)

print('Test loss:', score[0])
print('Test accuracy:', score[1])

y_pred_test = model.predict(x_test)

y_pred_test_b = [i[0] for i in y_pred_test]
y_test_b = [i[0] for i in y_test]

NN_FPR_test, NN_TPR_test, NN_TH_test = roc_curve(y_test_b, y_pred_test_b, pos_label=1, drop_intermediate=True) # Signal class set to 1

leakage_90 = NN_FPR_test[bisect.bisect_left(NN_TPR_test,0.9)]
print("Background Leakage at 90 Percent Signal Efficieny: %s" %(leakage_90))

plt.plot(NN_FPR_test, NN_TPR_test, label="Inside FV", color='orange')
plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('Background leakage (FPR)')
plt.ylabel('Signal efficiency (TPR)')
plt.legend()
plt.savefig('signal_eff.png')
plt.clf()


'''Save model '''

# directory="./saved_models_nn/"

# serialize model to JSON
# model_json = model.to_json()
# with open(directory+"fiducialize_z_S2_nn_model.json", "w") as json_file:
#     json_file.write(model_json)

# serialize weights to HDF5
# model.save_weights(directory+"fiducialize_z_S2_nn_weights.h5")

#save model architecture visualization
# ann_viz(model, title="S2 Fiducialization NN Architecture")
 
'''Load model again at later point in time'''
 
# # load json and create model
# json_file = open('model.json', 'r')
# loaded_model_json = json_file.read()
# json_file.close()
# loaded_model = model_from_json(loaded_model_json)
# # load weights into new model
# loaded_model.load_weights("model.h5")
# print("Loaded model from disk")
 
# # evaluate loaded model on test data
# loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
# score = loaded_model.evaluate(X, Y, verbose=0)
# print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1]*100))

'''Plot training & validation accuracies'''

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('model_accuracy.png')
plt.clf()

'''Plot training & validation loss values'''

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('model_loss.png')
plt.clf()