# import os
# import uproot
# import logging
import pickle
from scipy.integrate import cumtrapz
from numpy import *
# from time import process_time as timer


with open('/global/cfs/projectdirs/lz/users/jyjohnson/s2_pulses_5k_phd_v1.pickle', 'rb') as handle:
    s2_pulses_lib = pickle.load(handle)

ch_pos_map={}
ch_pos_map['channelID'], ch_pos_map['x_mm'], ch_pos_map['y_mm'] = loadtxt('/global/cfs/projectdirs/lz/users/jyjohnson/PMT_top_array_positions.txt',usecols=(0,2,3),skiprows=2,unpack=True)

LG=False #Are we using LG or HG channel waveforms?

final_matrix=[]
for event in range(len(s2_pulses_lib['channelWaveform'])):
    if s2_pulses_lib['pulseArea_phd'][event] < 500 or s2_pulses_lib['pulseArea_phd'][event] > 5000:
        continue
    sorted_waveforms=[x for _,x in sorted(zip(s2_pulses_lib['channelWaveformArea_phd'][event],s2_pulses_lib['channelWaveform'][event]), reverse=True)]
    sorted_chIDs=[x for _,x in sorted(zip(s2_pulses_lib['channelWaveformArea_phd'][event],s2_pulses_lib['channelIDNum'][event]), reverse=True)]
    sorted_waveform_areas=[x for x,_ in sorted(zip(s2_pulses_lib['channelWaveformArea_phd'][event],s2_pulses_lib['channelWaveform'][event]), reverse=True)]
        
    tmp_pulse_vector=array([])
    leading_ch_cut=(ch_pos_map['channelID']==sorted_chIDs[0])
    leading_PMT_x=ch_pos_map['x_mm'][leading_ch_cut]/10. #in cm
    leading_PMT_y=ch_pos_map['y_mm'][leading_ch_cut]/10. #in cm
    evt_flag=False
    for pulse in range(len(sorted_waveforms)):
        wfm_areas=cumtrapz(sorted_waveforms[pulse],dx=10)
        start_time_index=argwhere(wfm_areas<0.05*sorted_waveform_areas[pulse])
        if len(start_time_index)==0:
            fract_area=0
            start_time=-999
            evt_flag=True
        else:
            start_time=(start_time_index[-1][0]*10+s2_pulses_lib['pulseStartTime_ns'][event])/1000. #in us
            fract_area=sorted_waveform_areas[pulse]/s2_pulses_lib['pulseArea_phd'][event]
        
        tmp_pulse_vector=append(tmp_pulse_vector,start_time) #in us
        tmp_pulse_vector=append(tmp_pulse_vector,fract_area) 
        
        ch_cut=(ch_pos_map['channelID']==sorted_chIDs[pulse])
        tmp_x = ch_pos_map['x_mm'][ch_cut]/10. #in cm
        tmp_y = ch_pos_map['y_mm'][ch_cut]/10. #in cm
        radius_from_leading_PMT=sqrt((tmp_x - leading_PMT_x)**2 + (tmp_y - leading_PMT_y)**2)  #in cm
        tmp_pulse_vector=append(tmp_pulse_vector,radius_from_leading_PMT) #in cm
        
    if evt_flag==True:
        continue
    else:
        tmp_pulse_vector=append(tmp_pulse_vector,s2_pulses_lib['driftTime_ns'][event])
        final_matrix.append(tmp_pulse_vector)

final_matrix=array(final_matrix)
# final_matrix=transpose(final_matrix)
print("Shape of generated matrix: %s,%s" %(len(final_matrix),len(final_matrix[0])))

with open('/global/cfs/projectdirs/lz/users/jyjohnson/s2_pulses_5k_phd_input_nn_v4.pickle', 'wb') as handle:
    pickle.dump(final_matrix, handle, protocol=pickle.HIGHEST_PROTOCOL)